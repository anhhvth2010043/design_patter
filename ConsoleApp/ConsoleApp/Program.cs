﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{

    interface ISubject
    {
        void Attach(IObserver observer);
        void Notify();    
    
    }

    class WeatherStation : ISubject
    {

        private List<IObserver> _observers;
        private float _temperature;

        public float Temperature {
            get { return _temperature; } 
            set 
                { 
                    _temperature = value;
                    Notify();
                }
        
        }

        public WeatherStation()
        {
            _observers = new List<IObserver>();
        }

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Notify()
        {
            _observers.ForEach(o => {
                o.Update(this);
            });
        }
    }

   

    interface IObserver
    {
        void Update(ISubject subject);
    }


    class NewAgency : IObserver
    {
        public String AgencyName { get; set; }

        public NewAgency(String name)
        {
            AgencyName = name;
        }

        public void  Update(ISubject subject)
        {
            if (subject is WeatherStation weatherStation)
            {
                Console.WriteLine(String.Format("{0}  reportion remperature  {1} degree cecius",
                    AgencyName,
                    weatherStation.Temperature));
                Console.WriteLine();
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            WeatherStation weatherStation = new WeatherStation();

            NewAgency newAgency1 = new NewAgency("Alpha  News Agency");

            weatherStation.Attach(newAgency1);

            NewAgency newAgency2 = new NewAgency("Omega  News Agency 2");

            weatherStation.Attach(newAgency2);

            weatherStation.Temperature = 31.2f;
            weatherStation.Temperature = 52f;
            weatherStation.Temperature = 2f;
            weatherStation.Temperature = 1.2f;


            Console.ReadLine();
        }
    }
}
